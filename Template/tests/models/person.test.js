// eslint-disable-next-line no-unused-vars
import { Person } from '../../src/models/person.js';

describe('Person', () => {

  describe('constructor', () => {

    it('should throw if the passed name value is invalid', () => {
      expect(() => new Person('', 25, ['friend1', 'friend2'])).toThrow('Invalid name');
    });
    it('should throw if the passed age value is invalid', () => {
      expect(() => new Person('John Doe', -5, ['friend1', 'friend2'])).toThrow('Invalid age');
      expect(() => new Person('John Doe', 151, ['friend1', 'friend2'])).toThrow('Invalid age');
      expect(() => new Person('John Doe', '56', ['friend1', 'friend2'])).toThrow('Invalid age');
    });
    it('should throw if the passed friends value is invalid', () => {
      expect(() => new Person('John Doe', 30, 'invalid friends')).toThrow('Invalid friends list!');
    });
    it('should NOT throw if all the passed values are valid', () => {
      expect(() => new Person('John Doe', 30, ['friend1', 'friend2'])).not.toThrow();
    });
    it('should set the name correctly', () => {
      const person = new Person('John Doe', 30, ['friend1', 'friend2']);
      expect(person.name = 'John Does').toBe('John Does');
    });
    it('should set the age correctly', () => {
      const person = new Person('John Doe', 30, ['friend1', 'friend2']);
      expect(person.age).toBe(30);
    });
    it('should set the friend correctly', () => {
      const friends = ['friend1', 'friend2'];
      const person = new Person('John Doe', 30, friends);
      expect(person.friends).toEqual(friends);
      expect(person.friends).not.toBe(friends);
    });
  });

  describe('name', () => {
    it('setter should throw if the value is invalid', () => {
      const person = new Person('John Doe', 30, ['friend1', 'friend2']);
      expect(() => (person.name = '')).toThrow('Invalid name');
      expect(() => (person.name = 'a')).toThrow('Invalid name');
      expect(() => (person.name = 'a'.repeat(47))).toThrow('Invalid name');
    });
    it('setter should set the value correctly', () => {
      const person = new Person('John Doe', 30, ['friend1', 'friend2']);
      person.name = 'Jane Smith';
      expect(person.name).toBe('Jane Smith');
    });
  });

  describe('age', () => {
    it('setter should throw if the value is invalid', () => {
      const person = new Person('John Doe', 30, ['friend1', 'friend2']);
      expect(() => (person.age = -5)).toThrow('Invalid age');
      expect(() => (person.age = 151)).toThrow('Invalid age');
      expect(() => (person.age = '151')).toThrow('Invalid age');
    });
    it('setter should set the value correctly', () => {
      const person = new Person('John Doe', 30, ['friend1', 'friend2']);
      person.age = 35;
      expect(person.age).toBe(35);
    });
  });
  describe('friends', () => {
    it('should be readonly, i.e. throw if you try to set it to a new value', () => {
      const person = new Person('John Doe', 30, ['friend1', 'friend2']);
      expect(() => (person.friends = ['new friend'])).toThrow(TypeError);
    });
    it('should return a copy of the array, a different reference, but structurally the same', () => {
      const originalFriends = ['friend1', 'friend2'];
      const person = new Person('John Doe', 30, originalFriends);
      const returnedFriends = person.friends;
      expect(returnedFriends).toEqual(originalFriends);
      expect(returnedFriends).not.toBe(originalFriends);
    });
  });
  describe('printInfo', () => {
    it('should return the correct string', () => {
      const person = new Person('John Doe', 30, ['friend1', 'friend2']);
      const expectedString = 'Name: John Doe\nAge: 30\nFriend count: 2';
      expect(person.printInfo()).toBe(expectedString);
    });

  });
});
