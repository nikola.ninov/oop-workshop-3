import { Transformer } from '../../src/engine/transformer.js';

describe('Transformer', () => {
  it('should call the provided function for each element in the array', () => {
    // Arrange
    const transformer = new Transformer();
    const data = [1, 2, 3];
    const transformFn = jest.fn();
    // Act
    transformer.transform(data, transformFn);
    // Assert
    expect(transformFn).toHaveBeenCalledTimes(data.length);
    expect(transformFn).toHaveBeenCalledWith(expect.any(Number), expect.any(Number), expect.any(Array));
  });

  it('should return the transformed array', () => {
    // Arrange
    const transformer = new Transformer();
    const data = [1, 2, 3];
    const transformFn = jest.fn((item) => item * 2);
    // Act
    const transformedArray = transformer.transform(data, transformFn);
    // Assert
    expect(transformedArray).toEqual([2, 4, 6]);
  });
});
