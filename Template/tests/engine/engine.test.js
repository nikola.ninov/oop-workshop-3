import { Engine } from '../../src/engine/engine.js';
import { Logger } from '../../src/engine/logger.js';
import { Transformer } from '../../src/engine/transformer.js';
import { Person } from '../../src/models/person.js';


const testData =[
  {
    "name": "John",
    "age": 20,
    "friends": [
      "Anika",
      "Jane"
    ]
  },
  {
    "name": "George",
    "age": 25,
    "friends": []
  },
  {
    "name": "Anika",
    "age": 32,
    "friends": [
      "John"
    ]
  },
  {
    "name": "Jane",
    "age": 23,
    "friends": [
      "John",
      "Eric"
    ]
  },
  {
    "name": "Eric",
    "age": 29,
    "friends": [
      "Jane"
    ]
  }
];

describe('Engine', () => {
  let engine;

  beforeEach(() => {
    engine = new Engine();
  });

  describe('run', () => {
    it('should call the logWith method', () => {
      const transformFn = ({ name, age, friends }) => new Person(name, age, friends);
      const logWithFn = (data) => data.map(f => f.printInfo()).join('\n\n');

      // Mock console.log to capture the output
      const consoleLogSpy = jest.spyOn(console, 'log');
      consoleLogSpy.mockImplementation(() => {}); // to hide tests from console

      // used article https://dev.to/zirkelc/how-to-test-console-log-5fhd

      engine.run(transformFn, logWithFn);
      const expectedOutput = testData.map((person) => new Person(person.name, person.age, person.friends).printInfo()).join('\n\n');
      expect(consoleLogSpy).toHaveBeenCalledWith(expectedOutput);
      consoleLogSpy.mockRestore();
    });
    it('should call the readFile method', () => {
      const mockReader = new FileReader();
      mockReader.readFile = JSON.stringify(testData);
      engine = new Engine(mockReader);

      // Call the run method
      const transformFn = jest.fn();
      const logWithFn = jest.fn();
      engine.run(transformFn, logWithFn);

      // Verify that the readFile method was accessed
      expect(mockReader.readFile).toBe(JSON.stringify(testData));
    });
  });
});
