import { Transformer } from './transformer.js';
import { FileReader } from './file-reader.js';
import { Logger } from './logger.js';

export class Engine {

  #reader = new FileReader();
  #transformer = new Transformer();
  #logger = new Logger();

  run(transformFn, logWithFn) {
    const data = JSON.parse(this.#reader.readFile('data.json'));
    const transformedData = this.#transformer.transform(data, transformFn);
    this.#logger.logWith(transformedData, logWithFn);
  }

}
